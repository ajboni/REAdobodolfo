--[[
 * ReaScript Name: Multitrack export notes to SRT
 * Description: Export item's note selection on every track as different SRT files.
 * Instructions: Export will be relative to loop marker.
 * Authors: Rodobodolfo
 * Author URl: http://pointbleepstudios.com
 * License: 
 * Forum Thread: 
 * Forum Thread URl: 
 * Version: 0
 * Date: 2018/03/24
 * REAPER: 5.78
 * Extensions: None
]]

-- CleanUp the console
reaper.ShowConsoleMsg("");

-- Set up variables
projectIndex = 0;
totalTracks  = reaper.CountTracks(projectIndex);  -- totalNumberOfTacks
trackList = {};  -- list of tracks.
project_path = reaper.GetProjectPath(""); 
defaultExportExtension = ".txt";


-- Log a message to the console
function Log(msg)
	reaper.ShowConsoleMsg(msg .. '\n');
end


-- Get all tracks and their info
function GetTracksInformation() 
	
	_trackList = {};
	-- Get all notes on track.
	for i = 0,totalTracks -1 do 
		_trackList[i] = {};
		_trackList[i].Items = {};
		_trackList[i].Track = reaper.GetTrack(projectIndex, i);
		_trackList[i].TrackName = _trackName;
		_trackList[i].ItemCount = 0;
		_trackList[i].ExportedSRT = ""; -- Final SRT text.

		-- Get each track names.
		retval, _trackName = reaper.GetSetMediaTrackInfo_String(_trackList[i].Track, "P_NAME", "", false);
		_trackList[i].TrackName = _trackName;

		-- Save the item count
		_trackList[i].ItemCount = reaper.CountTrackMediaItems(_trackList[i].Track);
		
		-- Iterate through all track items and save useful properties.
		for j = 0, _trackList[i].ItemCount -1 do 
			_trackList[i].Items[j] = {};
			_item = reaper.GetTrackMediaItem(_trackList[i].Track, j);
			
			-- Save Item for future use
			_trackList[i].Items[j].Item = _item;		
			
			-- Useful properties
			_trackList[i].Items[j].Position = reaper.GetMediaItemInfo_Value(_item, "D_POSITION");
			_trackList[i].Items[j].Duration = reaper.GetMediaItemInfo_Value(_item, "D_LENGTH");			
			_trackList[i].Items[j].StartTime = ToTimecode(_trackList[i].Items[j].Position);
			_trackList[i].Items[j].EndTime = ToTimecode(_trackList[i].Items[j].Position + _trackList[i].Items[j].Duration);
			_trackList[i].Items[j].Notes = reaper.ULT_GetMediaItemNote(_item);
			
			-- Concatenate Notes and time codes
			_trackList[i].ExportedSRT = _trackList[i].ExportedSRT .. "\n" ;
			_trackList[i].ExportedSRT = _trackList[i].ExportedSRT .. _trackList[i].Items[j].StartTime;
			_trackList[i].ExportedSRT = _trackList[i].ExportedSRT .. " --> " ;
			_trackList[i].ExportedSRT = _trackList[i].ExportedSRT .. _trackList[i].Items[j].EndTime;
			_trackList[i].ExportedSRT = _trackList[i].ExportedSRT .. "\n" ;
			_trackList[i].ExportedSRT = _trackList[i].ExportedSRT .. _trackList[i].Items[j].Notes;
			_trackList[i].ExportedSRT = _trackList[i].ExportedSRT .. "\n" ;
			
			-- Remove linebreaks
			-- _trackList[i].Items[j].Notes = string.gsub(_trackList[i].Items[j].Notes, "\n", "");
			
		end

	end
	return _trackList;
end

-- Convert Reaper Item position format topp HH:MM:SS:MMMM format. 
-- Borrowed from "Export selection as SRT subtitles with offset"
-- http://forum.cockos.com/showthread.php?p=1495841#post1495841
function ToTimecode(position)
	hour = math.floor(position/3600)
	minute = math.floor((position - 3600*math.floor(position/3600)) / 60)
	second = math.floor(position - 3600*math.floor(position/3600) - 60*math.floor((position-3600*math.floor(position/3600))/60))
	millisecond = math.floor(1000*(position-math.floor(position)) )
	
	return string.format("%02d:%02d:%02d,%03d", hour, minute, second, millisecond)
end

-- Generates a SRT formated text.
function GenerateSRTExport() 
	local t = "";
	
	for i = 0,totalTracks -1 do 
		for j = 0, trackList[i].ItemCount -1 do 
			t = t .. trackList[i].Items[j].Notes;
		end	
	end

return t
end


-- Ask the user to folder where to save SRT file
function PromptUserForFolder() 

	_extension = "";
	retval, _exportPath = reaper.GetUserInputs("Export SRT", 2, "Full path of the folder:,Extension", project_path .. "," .. _extension); 
	if(_exportPath == "") then _exportPath = project_path end;
	if(_extension == "") then _extension = defaultExportExtension; end
	
	-- User cancel
	if (not retval) then return nil end 
	
	
	Log("HELLO");
	
end

---------------- Execute ------------------ 


PromptUserForFolder();

trackList = GetTracksInformation();
-- Log(trackList[1].Items[4].Notes);
--Log(trackList[2].ExportedSRT);
--Log(GenerateSRTExport());

