# Rodobodolfo's REAPER REAScripts.

## Multitrack SRT export.
It will export all tracks with notes into different SRT files.  
Timecode format:

```
2
00:00:12,500 --> 00:00:15,000
Line1
Line2

```

### Features:

- Do not export muted folders.
- Do not export tracks with no notes.
